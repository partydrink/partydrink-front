import React, { useState } from "react";
import { makeStyles, Grid, TextField, MenuItem, Button } from "@material-ui/core";
import { TextParameter, TextParameterOptions, textParametersList } from "../../../api/classes/question.class";
import i18next from "i18next";
// import { TextParameter } from '../../../api/classes/question.class';

const useStyles = makeStyles((theme) => ({
  selectParam: {
    // margin: 0,
  },
  addParamButtonContainer: {
    alignSelf: "center",
    paddingLeft: "20px",
  },
}));

interface OwnProps {
  onAddParameter: (parameterString: string) => void;
}

type Props = OwnProps;

const QuestionParam: React.FunctionComponent<Props> = (props: Props) => {
  const classes = useStyles();

  const [parameter, setParameter] = useState<TextParameter>(textParametersList[0]);

  const [parameterOptions, setParameterOptions] = useState<TextParameterOptions | undefined>(undefined);

  const handleParameterChange = (e: React.ChangeEvent<{ value: unknown }>) => {
    const newParameter = textParametersList!.find((p) => p.value === (e.target.value as string))!;

    setParameter(newParameter);

    setParameterOptions(newParameter.options);
  };

  const handleOptionParameterChange = (
    e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
    target: "min" | "max"
  ) => {
    if (parameterOptions && parameterOptions[target] !== undefined) {
      const newParameterOptions = { ...parameterOptions };

      const newValue = parseInt(e.target.value);

      if (!isNaN(newValue) && newValue >= 0 && newValue < 100) {
        newParameterOptions[target] = newValue;
      } else if (isNaN(newValue)) {
        newParameterOptions[target] = e.target.value as any;
      }

      setParameterOptions(newParameterOptions);
    }
  };

  const beforeAddParameter = () => {
    let paramString = ` {${parameter.value}`;

    if (parameterOptions) {
      const optionsEntries = Object.entries(parameterOptions);

      if (optionsEntries.length > 0) {
        paramString += "(";
        for (const optionEntry of optionsEntries) {
          paramString += `${optionEntry[0]}:${optionEntry[1]};`;
        }
        paramString = paramString.slice(0, -1) + ")";
      }
    }

    paramString += "}";

    props.onAddParameter(paramString);
  };

  return (
    <Grid container>
      <Grid item xs={8}>
        <TextField
          className={classes.selectParam}
          name="parameterSelect"
          margin="normal"
          variant="outlined"
          select
          fullWidth
          id="parameterSelect"
          label={i18next.t("TEXT_PARAMETER")}
          onChange={handleParameterChange}
          value={parameter?.value}
        >
          {textParametersList ? (
            textParametersList.map((p) => (
              <MenuItem value={p.value} key={p.value}>
                {i18next.t(p.text)}
              </MenuItem>
            ))
          ) : (
            <MenuItem disabled>{i18next.t("LOADING")}</MenuItem>
          )}
        </TextField>
      </Grid>
      <Grid item xs={4} className={classes.addParamButtonContainer}>
        <Button variant="contained" color="primary" fullWidth onClick={beforeAddParameter}>
          {i18next.t("ADD_PARAMETER")}
        </Button>
      </Grid>
      {parameterOptions && (
        <Grid container item xs={12} spacing={1}>
          {Object.entries(parameterOptions).map(([key, value]) => {
            return (
              <Grid item xs={6} key={key}>
                <TextField
                  fullWidth
                  margin="normal"
                  variant="outlined"
                  label={i18next.t("VALUE_" + key)}
                  type={typeof value === "number" ? "number" : "text"}
                  value={value}
                  onChange={(e) => handleOptionParameterChange(e, key as any)}
                />
              </Grid>
            );
          })}
        </Grid>
      )}
    </Grid>
  );
};

export default QuestionParam;
