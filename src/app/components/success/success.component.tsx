import React from "react";
import CloseIcon from "@material-ui/icons/Close";
import { IconButton } from "@material-ui/core";
import { WithSnackbarProps, withSnackbar } from "notistack";

import apiHandler from "../../../api/apiHandler";
import { Question } from "../../../api/classes/question.class";
import { Role } from "../../../api/classes/role.class";
import { QuestionType } from "../../../api/classes/questionType.class";
import { Game } from "../../../api/classes/game.class";
import { QuestionTemplate } from "../../../api/classes/questionTemplate.class";

import { ServiceNames, ServiceEvent, ServiceEvents } from "../../../api/services/baseService";
import i18next from "i18next";

interface OwnProps {}

type Props = OwnProps & WithSnackbarProps;

interface State {
  eventsLinked: ServiceEvent[];
}

class SuccessComponent extends React.Component<Props, State> {
  /**
   *
   */
  constructor(props: Props) {
    super(props);

    let eventsLinkedTmp = [
      this.addEvent("logged in", ServiceNames.User),
      this.addEvent("logged out", ServiceNames.User),
      this.addEvent("registered", ServiceNames.User),
      this.addEvent(ServiceEvents.created, ServiceNames.Question),
      this.addEvent(ServiceEvents.updated, ServiceNames.Question),
      this.addEvent(ServiceEvents.removed, ServiceNames.Question),
      this.addEvent(ServiceEvents.created, ServiceNames.Role),
      this.addEvent(ServiceEvents.updated, ServiceNames.Role),
      this.addEvent(ServiceEvents.removed, ServiceNames.Role),
      this.addEvent(ServiceEvents.created, ServiceNames.QuestionType),
      this.addEvent(ServiceEvents.updated, ServiceNames.QuestionType),
      this.addEvent(ServiceEvents.removed, ServiceNames.QuestionType),
      this.addEvent(ServiceEvents.created, ServiceNames.Game),
      this.addEvent(ServiceEvents.updated, ServiceNames.Game),
      this.addEvent(ServiceEvents.removed, ServiceNames.Game),
      this.addEvent(ServiceEvents.created, ServiceNames.QuestionTemplate),
      this.addEvent(ServiceEvents.updated, ServiceNames.QuestionTemplate),
      this.addEvent(ServiceEvents.removed, ServiceNames.QuestionTemplate),
    ];

    this.state = {
      eventsLinked: eventsLinkedTmp.filter((e) => e.type !== "none"),
    };
  }

  addEvent(type: string, name: ServiceNames) {
    let service = apiHandler.service(name);

    if (service) {
      service.ownEvents.on(type, (this as any)["on" + name](type));

      return { type, name };
    }

    return { type: "none", name };
  }

  removeEvent(type: string, name: ServiceNames) {
    let service = apiHandler.service(name);

    if (service) {
      service.ownEvents.off(type, (this as any)["on" + name](type));
    }
  }

  onQuestion(type: string) {
    return (question: Question) => {
      this.displaySnackbar(`${i18next.t("QUESTION")} : ${i18next.t(type.toUpperCase())}`);
    };
  }

  onRole(type: string) {
    return (role: Role) => {
      this.displaySnackbar(`${i18next.t("ROLE")} : ${i18next.t(type.toUpperCase())}`);
    };
  }

  onQuestionType(type: string) {
    return (questionType: QuestionType) => {
      this.displaySnackbar(`${i18next.t("QUESTION_TYPE")} : ${i18next.t(type.toUpperCase())}`);
    };
  }

  onGame(type: string) {
    return (game: Game) => {
      this.displaySnackbar(`${i18next.t("GAME")} : ${i18next.t(type.toUpperCase())}`);
    };
  }

  onQuestionTemplate(type: string) {
    return (questionTemplate: QuestionTemplate) => {
      this.displaySnackbar(`${i18next.t("QUESTION_TEMPLATE")} : ${i18next.t(type.toUpperCase())}`);
    };
  }

  onUser(type: string) {
    return () => {
      console.log(type);

      this.displaySnackbar(`${i18next.t("USER")} : ${i18next.t(type.toUpperCase())}`);
    };
  }

  componentWillUnmount() {
    this.state.eventsLinked.forEach((e) => {
      this.removeEvent(e.type, e.name);
    });
  }

  displaySnackbar(message: string) {
    this.props.enqueueSnackbar(<span>{message}</span>, {
      autoHideDuration: 3000,
      variant: "success",
      action: (key) => {
        return (
          <IconButton key="close" aria-label="close" color="inherit" onClick={() => this.props.closeSnackbar(key)}>
            <CloseIcon />
          </IconButton>
        );
      },
    });
  }

  render() {
    return <></>;
  }
}

export const Success = withSnackbar(SuccessComponent);
