import React, { useEffect, useState } from "react";
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  makeStyles,
  Theme,
} from "@material-ui/core";
import Cookies from "universal-cookie";

const useStyles = makeStyles((theme: Theme) => ({
  backDrop: {
    backdropFilter: "blur(8px)",
    backgroundColor: "rgba(0,0,30,0.4)",
  },
}));

const cookies = new Cookies();

export default function TermsModal() {
  const [visible, setVisible] = useState(false);

  useEffect(() => {
    if (!cookies.get("termsUtilisation")) {
      show();
    }
  }, []);

  function submits() {
    cookies.set("termsUtilisation", "true", { path: "/", expires: new Date(Date.now() + 1000 * 60 * 60 * 12) });
  }

  function show() {
    setVisible(true);
  }

  function hide() {
    setVisible(false);
  }

  function handleAccept() {
    submits();
    hide();
  }

  const classes = useStyles();
  return (
    <Dialog
      open={visible}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
      BackdropProps={{
        classes: {
          root: classes.backDrop,
        },
      }}
    >
      <DialogTitle id="alert-dialog-title">Avertissement</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          L'abus d'alcool est dangereux pour la santé, consommez avec modération. En acceptant cet avertissement, vous
          confirmez être responsable des éventuelles conséquences pouvant être liées à l'utilisation de PartyDrink.
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleAccept} color="primary" variant="contained">
          J'accepte
        </Button>
      </DialogActions>
    </Dialog>
  );
}
