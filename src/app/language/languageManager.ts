import i18next from "i18next";
import { initReactI18next } from "react-i18next";

export interface Language {
  country: string;
  countryCode: string;
  language: string;
  languageCode: string;
  iconFileName: string;
}

export const availableLanguages: Language[] = [
  {
    country: "France",
    countryCode: "FR",
    language: "Français",
    languageCode: "fr",
    iconFileName: "195-france.svg",
  },
  {
    country: "Great Britain",
    countryCode: "GB",
    language: "English",
    languageCode: "en",
    iconFileName: "260-united-kingdom.svg",
  },
];

class LanguageManager {
  public static DefaultLanguage: Language = availableLanguages[0];

  private _language: Language;

  /**
   *
   */
  constructor() {
    const languageObject = localStorage.getItem("language");

    if (languageObject != null) {
      this._language = JSON.parse(languageObject) as Language;
    } else {
      this._language = LanguageManager.DefaultLanguage;

      localStorage.setItem("language", JSON.stringify(this._language));
    }
  }

  private async importLanguage(language: Language) {
    const languageModule = await import(`../../i18n/${language.languageCode}-${language.countryCode}.json`);

    const languageValues = languageModule.default;

    return languageValues;
  }

  public async initLanguages() {
    const i18nOptions: any = {
      resources: {},
      interpolation: {
        escapeValue: false,
      },
    };

    for (const l of availableLanguages) {
      const languageObject = await this.importLanguage(l);

      if (languageObject) {
        i18nOptions.resources[l.languageCode] = {
          translation: languageObject,
        };
      }
    }

    i18next
      .use(initReactI18next) // passes i18n down to react-i18next
      .init(i18nOptions);

    i18next.changeLanguage(this._language.languageCode);
  }

  public setLanguage(language: Language) {
    const newLanguage = availableLanguages.find((l) => l.languageCode === language.languageCode);

    if (newLanguage) {
      this._language = newLanguage;

      localStorage.setItem("language", JSON.stringify(this._language));

      i18next.changeLanguage(this._language.languageCode);
    } else {
      throw new Error("Language not found");
    }
  }

  public getActualLanguage(): Language {
    return this._language;
  }
}

export const languageManager = new LanguageManager();
