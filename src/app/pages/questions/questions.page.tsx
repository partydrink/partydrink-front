import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { ThunkDispatch } from "redux-thunk";
import { Box, Fab } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import AddIcon from "@material-ui/icons/Add";

import { QuestionsState } from "../../../store/questions/types";
import { RootState } from "../../../store";
import { QuestionsActions } from "../../../store/questions/actions";
import { Question } from "../../../api/classes/question.class";
import { Loading } from "../../components/utils/loading.component";
import { QuestionTypesState } from "../../../store/questionTypes/types";
import { QuestionList } from "../../components/question/question.list.component";
import { yesNoController } from "../../components/dialogs/yesno.component";
import { QuestionDialog } from "../../components/question/question.dialog.component";
import { UserState } from "../../../store/user/types";
import i18next from "i18next";

const useStyles = makeStyles((theme) => ({
  root: {
    paddingBottom: theme.spacing(10),
  },
  centerButton: {
    textAlign: "center",
  },
}));

interface OwnProps {}

interface DispatchProps {
  questionCreate: (question: Question) => Promise<any>;
  questionUpdate: (question: Question) => Promise<any>;
  questionRemove: (questionId: string) => Promise<any>;
  questionGetNexts: () => Promise<any>;
  getUserQuestions: (userId: string) => Promise<any>;
}

interface StateProps {
  questionsState: QuestionsState;
  questionTypesState: QuestionTypesState;
  userState: UserState;
}

type Props = StateProps & OwnProps & DispatchProps;

interface ModalProps {
  open: boolean;
  question: Question;
  title?: string;
  onAccept?: (question: Question) => void;
  submitButtonText?: string;
}
const QuestionsPage: React.FunctionComponent<Props> = (props: Props) => {
  const classes = useStyles();
  const user = props.userState.user!;

  useEffect(() => {
    if (!props.questionsState.questions || props.questionsState.questions.length === 0) {
      // handleOnGetNext();
      getUserQuestions();
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (props.questionsState.questions) {
      setUserQuestions(props.questionsState.questions.filter((q) => q.creator.id.toString() === user.id.toString()));

      setOtherQuestions(props.questionsState.questions.filter((q) => q.creator.id.toString() !== user.id.toString()));
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.questionsState.questions]);

  const [modalProps, setModalProps] = useState({
    open: false,
    question: Question.New({}),
  } as ModalProps);

  const [userQuestions, setUserQuestions] = useState<Question[]>([]);
  const [otherQuestions, setOtherQuestions] = useState<Question[]>([]);

  const openModal = () => setModalProps({ ...modalProps, open: true }); // eslint-disable-line

  const closeModal = () => setModalProps({ ...modalProps, open: false });

  const handleCreate = (createdQuestion: Question) => {
    closeModal();

    props.questionCreate(createdQuestion);
  };

  const handleOnCreate = () => {
    setModalProps({
      open: true,
      onAccept: handleCreate,
      question: Question.New({}),
      title: i18next.t("CREATE_A_NEW_QUESTION"),
      submitButtonText: i18next.t("CREATE"),
    });
  };

  const handleOnUpdate = (clickedQuestion: Question) => {
    props.questionUpdate(clickedQuestion);
  };

  const handleOnDelete = (questionId: string) => {
    yesNoController()!
      .present({
        title: i18next.t("DELETE_QUESTION_CONFIRMATION"),
        acceptText: i18next.t("YES"),
        denyText: i18next.t("NO"),
      })
      .then(() => {
        props.questionRemove(questionId);
      })
      .catch((error) => {
        //this.props.addError(error);
      });
  };

  const getNextQuestions = () => {
    props.questionGetNexts().then((res) => {
      if (res) {
        getNextQuestions();
      } else {
        console.log("all questions loaded");
      }
    });
  };

  const getUserQuestions = () => {
    if (!props.questionsState.loading) {
      props.getUserQuestions(user.id.toString()).then(() => {
        getNextQuestions();
      });
    }
  };

  return (
    <>
      <Box component="div" className={classes.root}>
        {props.questionsState.questions ? (
          <>
            {userQuestions.length > 0 && (
              <QuestionList
                questions={userQuestions}
                onUpdate={handleOnUpdate}
                onDelete={handleOnDelete}
                title={i18next.t("YOUR_QUESTIONS")}
              />
            )}
            <QuestionList
              questions={otherQuestions}
              onUpdate={handleOnUpdate}
              onDelete={handleOnDelete}
              title={i18next.t("OTHER_QUESTIONS")}
            />
            {props.questionsState.loading && <Loading />}
          </>
        ) : (
          <Loading />
        )}
      </Box>

      <Fab className="floating-action-button" onClick={handleOnCreate} style={{ zIndex: 10 }}>
        <AddIcon />
      </Fab>

      <QuestionDialog
        dialogProps={{ open: modalProps.open, onClose: closeModal }}
        question={modalProps.question}
        editable
        title={modalProps.title}
        acceptButtonText={modalProps.submitButtonText}
        onAccept={modalProps.onAccept}
      />
    </>
  );
};

const mapStateToProps = (states: RootState, ownProps: OwnProps): StateProps => {
  return {
    questionsState: states.questionsState,
    questionTypesState: states.questionTypesState,
    userState: states.userState,
  };
};

const mapDispatchToProps = (dispatch: ThunkDispatch<{}, {}, any>, ownProps: OwnProps): DispatchProps => {
  return {
    questionCreate: async (question: Question) => {
      return await dispatch(QuestionsActions.createQuestion(question));
    },
    questionUpdate: async (question: Question) => {
      return await dispatch(QuestionsActions.updateQuestion(question));
    },
    questionRemove: async (questionId: string) => {
      return await dispatch(QuestionsActions.removeQuestion(questionId));
    },
    questionGetNexts: async () => {
      return await dispatch(QuestionsActions.getNextQuestions(20));
    },
    getUserQuestions: async (userId: string) => {
      return await dispatch(QuestionsActions.getUserQuestions(userId));
    },
  };
};

export const Questions = connect<StateProps, DispatchProps, OwnProps, RootState>(
  mapStateToProps,
  mapDispatchToProps
)(QuestionsPage);
