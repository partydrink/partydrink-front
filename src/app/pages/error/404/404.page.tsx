import i18next from "i18next";
import React from "react";

const Page404: React.FunctionComponent = () => {
  return <>{i18next.t("PAGE_NOT_FOUND")}</>;
};

export const PageNotFound = Page404;
