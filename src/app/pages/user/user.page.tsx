import { Box, FormControlLabel, makeStyles, Switch, Typography } from "@material-ui/core";
import i18next from "i18next";
import React from "react";
import { availableLanguages, languageManager } from "../../language/languageManager";
import { ThemeController } from "../../theme/themeManager";

const useStyles = makeStyles((theme) => ({
  root: {
    paddingBottom: theme.spacing(10),
    paddingTop: theme.spacing(1),
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2),
  },
  languagesContent: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  flagsLanguages: {
    width: "40px",
    display: "inline-block",
    marginLeft: "10px",
    opacity: "0.2",
    cursor: "pointer",
  },
  opacity: {
    opacity: 1,
    cursor: "inherit",
  },
}));

const themeStyle = makeStyles((theme) => ({
  root: {
    display: "flex",
    justifyContent: "space-between",
    marginLeft: "0",
  },
}));

export const UserPage = () => {
  const [, updateState] = React.useState();
  //@ts-ignore
  const forceUpdate = React.useCallback(() => updateState({}), []);

  const classes = useStyles();
  const checkBoxStyle = themeStyle();

  return (
    <>
      <Box component="div" className={classes.root}>
        <Typography component="h2" variant="h5" align="center">
          {i18next.t("USER_SETTINGS")}
        </Typography>
        <div className={classes.languagesContent}>
          <Typography component="label">{i18next.t("LANGUAGE")}</Typography>
          <div>
            {availableLanguages.map((l) => (
              <img
                alt="Language flag"
                src={`/assets/flags/${l.iconFileName}`}
                className={
                  classes.flagsLanguages +
                  " " +
                  (l.languageCode === languageManager.getActualLanguage().languageCode ? classes.opacity : "")
                }
                onClick={() => {
                  languageManager.setLanguage(l);
                  forceUpdate();
                }}
                key={l.iconFileName}
              />
            ))}
          </div>
        </div>
        <FormControlLabel
          value="start"
          control={
            <Switch onChange={ThemeController.toggleTheme} color="secondary" checked={ThemeController.isDark()} />
          }
          label={i18next.t("DARK_THEME")}
          labelPlacement="start"
          classes={checkBoxStyle}
        />
      </Box>
    </>
  );
};
