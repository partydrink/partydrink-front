import React from "react";
import { Container, Typography, makeStyles } from "@material-ui/core";
import { withRouter, RouteComponentProps } from "react-router";
import i18next from "i18next";

const useStyles = makeStyles((theme) => ({
  root: {},
  text: {
    marginTop: theme.spacing(5),
  },
}));

interface OwnProps {
  displayId?: string;
}

type Props = OwnProps & RouteComponentProps;

const GameNotFoundPage: React.FunctionComponent<Props> = (props: Props) => {
  const classes = useStyles();

  const displayId = props.displayId || (props.location.state! as any).displayId;

  return (
    <Container maxWidth="md" className={classes.root}>
      <Typography variant="h5" align="center" className={classes.text}>
        {i18next.t("ERROR_GAME_DISPLAY_ID_1")} <b>{displayId}</b>
        {i18next.t("ERROR_GAME_DISPLAY_ID_2")}
      </Typography>
    </Container>
  );
};

export const GameNotFound = withRouter(GameNotFoundPage);
