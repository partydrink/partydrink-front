import React from "react";
import { Box, makeStyles, Typography, IconButton } from "@material-ui/core";

import { TemplateDisplayProps } from "../templateDisplay";
import { BaseAvatar } from "../../components/utils/avatars.component";
import i18next from "i18next";

const useStyles = makeStyles((theme) => ({
  root: {
    textAlign: "center",
    height: "calc(100vh - 64px)",
    display: "flex",
    [`@media (orientation: portrait)`]: {
      flexDirection: "column",
    },
  },
  panel: {
    justifyContent: "center",
    display: "flex",
    flexDirection: "column",
    flex: 1,
  },
  userImg: {
    width: "50%",
    height: 0,
    paddingBottom: "50%",
  },
  questionText: {
    display: "flex",
    justifyContent: "space-evenly",
    flexDirection: "column",
    flex: 1,
  },
  questionActions: {
    display: "flex",
    flexDirection: "row",
    flex: 1,
  },
  questionActionButton: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    flex: 1,
    "& .MuiSvgIcon-root": {
      width: "2em",
      height: "2em",
    },
  },
  typeDisplay: {
    flex: 1.5,
    display: "flex",
    alignSelf: "center",
    flexDirection: "column",
    paddingBottom: 15,
  },
  animation: {
    width: "100%",
  },
  partydrinkFont: {
    fontFamily: "scriptoramamarkdownjfregular",
  },
}));

interface OwnProps {}

type Props = OwnProps & TemplateDisplayProps;

const SimpleQuestionTemplate: React.FunctionComponent<Props> = (props: Props) => {
  const classes = useStyles();

  const playingGame = props.playingGame;
  const actualQuestion = props.question;
  const actualPlayer = playingGame.getActualplayer();

  return (
    <Box component="div" className={classes.root}>
      <Box component="div" className={classes.panel}>
        {actualPlayer && (
          <>
            <BaseAvatar src={`/assets/logos/${actualQuestion.type.icon}.png`} percentSize={65} />
            {/* <TypeAvatar src="" type={actualQuestion.type} percentSize={65}></TypeAvatar> */}
            <Typography variant="h5" component="h3" className={classes.partydrinkFont}>
              {actualPlayer.name}
            </Typography>
          </>
        )}
      </Box>
      <Box component="div" className={classes.panel}>
        <Box className={classes.questionText} px={3}>
          {actualQuestion.displayDrinks && (
            <Typography variant="body1">
              {`
                ${i18next.t("FOR")} 
                ${playingGame.getNbDrinks()} 
                ${i18next.t("DRINKS")}
              `}
            </Typography>
          )}

          <Typography align="center" variant="body1">
            {playingGame.getActualQuestionText()}
          </Typography>
        </Box>
        <Box className={classes.questionActions}>
          <Box className={classes.questionActionButton}>
            <IconButton style={{ color: "#CF2A28" }} onClick={() => props.onDeny!(actualQuestion)!}>
              ❌
            </IconButton>
          </Box>
          <Box className={classes.typeDisplay}>
            {/* <TypeAvatar
              type={actualQuestion.type}
              src="https://via.placeholder.com/450?text=Type%20image"
              percentSize={65}
            /> */}
            {actualQuestion.type.icon && (
              <img
                src={`/assets/animations/${actualQuestion.type.icon}.gif`}
                alt="Type animation"
                className={classes.animation}
              ></img>
            )}
            {/* <video
              src="/assets/animations/action-unscreen.gif"
              autoPlay
              loop
              className={classes.animation}
              muted
            ></video> */}
            {/* <Typography className={classes.partydrinkFont}> {actualQuestion.type.name}</Typography> */}
          </Box>

          <Box className={classes.questionActionButton}>
            <IconButton style={{ color: "#6BA84F" }} onClick={() => props.onAccept!(actualQuestion)}>
              ✔️
            </IconButton>
          </Box>
        </Box>
      </Box>
    </Box>
  );
};

export default SimpleQuestionTemplate;
