import React from "react";
import { v4 as uuidV4 } from "uuid";
import { TextField, Grid, Typography, Box, makeStyles, FormControlLabel, Checkbox } from "@material-ui/core";

import Rating from "@material-ui/lab/Rating";

import { TemplateFormProps } from "../templateForm";
import { OutlinedDiv } from "../../components/utils/outlinedDiv.component";
import { UserItem } from "../../components/user/user.item.component";
import QuestionParam from "../../components/question/question.params.component";
import { textParametersRegex } from "../../../api/classes/question.class";
import i18next from "i18next";

const useStyles = makeStyles((theme) => ({
  ratingsGrid: {
    width: "calc(100% + 16px)",
    marginLeft: -8,
    textAlign: "center",
  },
  hotLevelRating: {
    color: "#FD6C9E",
  },
}));

interface OtherProps {}

type Props = OtherProps & TemplateFormProps;

const GroupQuestionTemplate: React.FunctionComponent<Props> = (props: Props) => {
  const classes = useStyles();

  const handleTextChange = (e: React.ChangeEvent<HTMLInputElement>) =>
    props.editable && props.formProps.setText(e.target.value);

  const handleDifficultyChange = (e: React.ChangeEvent<any>, value: number | null) =>
    props.editable && value != null && props.formProps.setDifficulty(value);

  const handleHotLevelChange = (e: React.ChangeEvent<any>, value: number | null) =>
    props.editable && value != null && props.formProps.setHotLevel(value);

  const handleDisplayDrinksChange = (e: React.ChangeEvent<HTMLInputElement>, value: boolean) =>
    props.editable && props.formProps.setDisplayDrinks(value);

  const handleAddParameter = (newParameter: string) => {
    props.formProps.setText(props.formProps.text + newParameter);

    setTimeout(() => {
      console.log(props.formProps.text.match(textParametersRegex));
    }, 500);
  };

  return (
    <>
      {props.displayText && (
        <TextField
          name={`${uuidV4()}_text`}
          margin="normal"
          variant="outlined"
          type="text"
          disabled={props.disabled}
          fullWidth
          multiline
          rows={4}
          rowsMax={6}
          id="text"
          label={i18next.t("TEXT")}
          value={props.formProps.text}
          onChange={handleTextChange}
        />
      )}

      {props.displayText && props.editable && props.type.allowParameters && (
        <QuestionParam onAddParameter={handleAddParameter} />
      )}

      <FormControlLabel
        control={
          <Checkbox
            disabled={props.disabled}
            readOnly={!props.editable}
            checked={props.formProps.displayDrinks}
            onChange={handleDisplayDrinksChange}
            name="displayDrinks"
          />
        }
        label={i18next.t("RANDOM_DRINKS")}
      />
      <Grid container className={classes.ratingsGrid}>
        <Grid item xs={6}>
          <Typography component="legend" color={props.disabled ? "textSecondary" : "initial"}>
            {i18next.t("DIFFICULTY")}
          </Typography>
          <Rating
            disabled={props.disabled}
            readOnly={!props.editable}
            name={`${uuidV4()}_difficulty`}
            value={props.formProps.difficulty}
            onChange={handleDifficultyChange}
            icon={"⭐"}
          />
        </Grid>
        <Grid item xs={6}>
          <Typography component="legend" color={props.disabled ? "textSecondary" : "initial"}>
            {i18next.t("HOT_LEVEL")}
          </Typography>
          <Rating
            disabled={props.disabled}
            readOnly={!props.editable}
            name={`${uuidV4()}_hotLevel`}
            className={classes.hotLevelRating}
            value={props.formProps.hotLevel}
            onChange={handleHotLevelChange}
            icon={"❤️"}
          />
        </Grid>
      </Grid>
      {props.displayExtraInfos && props.question && (
        <>
          <OutlinedDiv label={i18next.t("CREATOR")} disabled={props.disabled} fullWidth>
            <UserItem user={props.question.creator} />
          </OutlinedDiv>
          <OutlinedDiv label={i18next.t("CREATION_DATE")} disabled={props.disabled} fullWidth>
            <Box m={1}>{props.question.creationDate.toLocaleDateString("fr-FR", props.dateOptions)}</Box>
          </OutlinedDiv>
          <OutlinedDiv label={i18next.t("UPDATE_DATE")} disabled={props.disabled} fullWidth>
            <Box m={1}>{props.question.updateDate.toLocaleDateString("fr-FR", props.dateOptions)}</Box>
          </OutlinedDiv>
        </>
      )}
    </>
  );
};

export default GroupQuestionTemplate;
