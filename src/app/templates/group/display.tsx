import React from "react";
import { Box, makeStyles, Typography, Button } from "@material-ui/core";

import { TemplateDisplayProps } from "../templateDisplay";
import { TypeAvatar } from "../../components/utils/avatars.component";
import i18next from "i18next";

const useStyles = makeStyles((theme) => ({
  root: {
    textAlign: "center",
    height: "calc(100vh - 64px)",
    display: "flex",
    [`@media (orientation: portrait)`]: {
      flexDirection: "column",
    },
  },
  panel: {
    justifyContent: "center",
    display: "flex",
    flexDirection: "column",
    flex: 1,
  },
  userImg: {
    width: "50%",
    height: 0,
    paddingBottom: "50%",
  },
  questionText: {
    display: "flex",
    justifyContent: "space-evenly",
    flexDirection: "column",
    flex: 1,
  },
  questionActions: {
    display: "flex",
    flexDirection: "row",
    flex: 1,
  },
  questionActionButton: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    flex: 1,
    "& .MuiSvgIcon-root": {
      width: "2em",
      height: "2em",
    },
  },
  typeDisplay: {
    flex: 2,
    display: "flex",
    alignSelf: "center",
  },
}));

interface OwnProps {}

type Props = OwnProps & TemplateDisplayProps;

const GroupQuestionTemplate: React.FunctionComponent<Props> = (props: Props) => {
  const classes = useStyles();

  const playingGame = props.playingGame;
  const actualQuestion = props.question;
  // const actualPlayer = playingGame.getActualplayer();

  return (
    <Box component="div" className={classes.root}>
      <Box component="div" className={classes.panel}>
        <TypeAvatar src={`/assets/dummyGroup.png`} type={actualQuestion.type} percentSize={65}></TypeAvatar>

        <Typography variant="h6" component="h3">
          {i18next.t("GROUP_QUESTION")}
        </Typography>
      </Box>
      <Box component="div" className={classes.panel}>
        <Box className={classes.questionText} px={3}>
          {actualQuestion.displayDrinks && (
            <Typography variant="body1">
              {`
                ${i18next.t("FOR")} 
                ${playingGame.getNbDrinks()} 
                ${i18next.t("DRINKS")}
              `}
            </Typography>
          )}
          <Typography align="center" variant="body1">
            {playingGame.getActualQuestionText()}
          </Typography>
        </Box>
        <Box className={classes.questionActions}>
          <Box className={classes.questionActionButton}>
            <Button
              variant="contained"
              color="primary"
              style={{ width: "80%" }}
              onClick={() => props.onNext!(actualQuestion)}
            >
              {i18next.t("NEXT_QUESTION")}
            </Button>
          </Box>
        </Box>
      </Box>
    </Box>
  );
};

export default GroupQuestionTemplate;
