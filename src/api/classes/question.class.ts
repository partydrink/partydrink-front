import { QuestionType, QuestionTypeBackModel } from "./questionType.class";
import { User, UserBackModel } from "./user.class";

export interface QuestionBackModel {
  _id: string;
  _type: QuestionTypeBackModel;
  _text: string;
  _difficulty: number;
  _hotLevel: number;
  _creator: UserBackModel;
  _creationDate: string;
  _updateDate: string;
  _displayDrinks: boolean;
}

export class Question {
  public id: string = "";

  public type: QuestionType = new QuestionType();

  public text: string = "";

  public difficulty: number = 0;

  public hotLevel: number = 0;

  public creator: User = new User();

  public creationDate: Date = new Date();

  public updateDate: Date = new Date();

  public displayDrinks: boolean = false;

  public static New(datas: Partial<Question>): Question {
    return Object.assign(new Question(), datas);
  }

  public static fromBack(datas: QuestionBackModel) {
    let newObj = new Question();

    newObj.id = datas._id;
    newObj.type = QuestionType.fromBack(datas._type);
    newObj.text = datas._text;
    newObj.difficulty = datas._difficulty;
    newObj.hotLevel = datas._hotLevel;
    newObj.creator = User.fromBack(datas._creator);
    newObj.creationDate = new Date(datas._creationDate);
    newObj.updateDate = new Date(datas._updateDate);
    newObj.displayDrinks = datas._displayDrinks;

    return newObj;
  }

  public static CompareArrays(arr1: Question[], arr2: Question[]): boolean {
    return arr1.length === arr2.length && arr1.every((e, i) => Question.CompareObjects(e, arr2[i]));
  }

  public static CompareObjects(obj1: Question, obj2: Question): boolean {
    return (
      obj1 !== undefined &&
      obj2 !== undefined &&
      obj1.id === obj2.id &&
      obj1.difficulty === obj2.difficulty &&
      obj1.hotLevel === obj2.hotLevel &&
      obj1.text === obj2.text &&
      QuestionType.CompareObjects(obj1.type, obj2.type) &&
      User.CompareObjects(obj1.creator, obj2.creator) &&
      obj1.creationDate === obj2.creationDate &&
      obj1.updateDate === obj2.updateDate &&
      obj1.displayDrinks === obj2.displayDrinks
    );
  }
}

export interface TextParameter {
  text: string;
  value: string;
  playerParameter?: boolean;
  options?: TextParameterOptions;
}

export interface TextParameterOptions {
  min?: number;
  max?: number;
}

export const textParametersList: TextParameter[] = [
  {
    text: "RANDOM_PLAYER",
    value: "player",
    playerParameter: true,
  },
  {
    text: "RANDOM_GIRL_PLAYER",
    value: "playerGirl",
    playerParameter: true,
  },
  {
    text: "RANDOM_MAN_PLAYER",
    value: "playerMan",
    playerParameter: true,
  },
  {
    text: "RANDOM_NUMBER",
    value: "number",
    options: {
      min: 0,
      max: 5,
    },
  },
];

const reg = textParametersList
  .reduce((regexString, textParameter) => {
    if (textParameter.options) {
      const optionRegexStr = Object.entries(textParameter.options)
        .reduce((r, option) => {
          if (typeof option[1] === "number") {
            return `${r}${option[0]}:[0-9]+;`;
          } else if (typeof option[1] === "string") {
            return `${r}${option[0]}:[a-zA-Z]+;`;
          } else {
            return "";
          }
        }, "")
        .slice(0, -1);
      return `${regexString}{${textParameter.value}\\(${optionRegexStr}\\)}|`;
    } else {
      return `${regexString}{${textParameter.value}}|`;
    }
  }, "")
  .slice(0, -1);

// console.log(reg);

// console.log("{player}".replace(/[\{]/g, "'"));

export const textParametersRegex: RegExp = new RegExp(reg, "g");
