import seedRandom from "seedrandom";

import { QuestionType, QuestionTypeBackModel } from "./questionType.class";
import { User, UserBackModel } from "./user.class";
import { GameType, GameTypeBackModel } from "./gameType.class";
import { DummyUser } from "./dummyUser.class";
import { Helper } from "../../helper";
import {
  Question,
  QuestionBackModel,
  textParametersRegex,
} from "./question.class";
import { Score } from "./score.class";

export interface GameBackModel {
  _id: string;
  _displayId: string;
  _name: string;
  _users: UserBackModel[];
  _nbTurns: number;
  _actualTurn: number;
  _questionTypes: QuestionTypeBackModel[];
  _maxDifficulty: number;
  _maxHotLevel: number;
  _creator: UserBackModel;
  _type: GameTypeBackModel;
  _scores: Score[];
  _actualQuestion: QuestionBackModel;
}

export enum GameStatus {
  created = "Created",
  started = "Started",
  finished = "Finished",
}

export enum GameStatusColors {
  Created = "default",
  Started = "primary",
  Finished = "secondary",
}

export class Game {
  public id: string = "";

  public displayId: string = "";

  public name: string = "";

  public users: User[] = [];

  public dummyUsers: DummyUser[] = [];

  public nbTurns: number = 0;

  public actualTurn: number = 0;

  public questionTypes: QuestionType[] = [];

  public maxDifficulty: number = 0;

  public maxHotLevel: number = 0;

  public creator: User = new User();

  public creationDate: Date = new Date();

  public type: GameType = new GameType();

  public status: GameStatus = GameStatus.created;

  public scores: Score[] = [];

  public actualQuestion: Question = new Question();

  public get allUsers() {
    return [...this.users, ...this.dummyUsers];
  }

  public canStart(): boolean {
    if (this.allUsers.length <= 1) return false;
    return true;
  }

  public getActualPlayerIndex(): number {
    return Helper.getPlayer(
      this.actualTurn,
      this.nbTurns,
      this.allUsers.length,
      this.displayId
    );
  }

  public getActualplayer(): User | DummyUser {
    let userIndex = Helper.getPlayer(
      this.actualTurn,
      this.nbTurns,
      this.allUsers.length,
      this.displayId
    );

    return this.allUsers[userIndex];
  }

  private get rng(): seedrandom.prng {
    return seedRandom(this.id.toString() + this.actualTurn.toString());
  }

  public getActualQuestionText(): string {
    const textParameters = this.actualQuestion.text.match(textParametersRegex);
    let questionText = this.actualQuestion.text;

    if (textParameters) {
      try {
        const choosedPlayers = [this.getActualplayer()];

        for (const textParameter of textParameters) {
          const textParameterT = textParameter.split("(");

          const textParameterName = textParameterT[0].replace(/[{}]/g, "");

          switch (textParameterName) {
            case "player":
              const choosedPlayer = this.getRandomPlayer(choosedPlayers);

              choosedPlayers.push(choosedPlayer);

              questionText = questionText.replace(
                textParameter,
                choosedPlayer.name
              );

              break;
            case "playerGirl":
              const choosedPlayerGirl = this.getRandomPlayerGirl(
                choosedPlayers
              );

              choosedPlayers.push(choosedPlayerGirl);

              questionText = questionText.replace(
                textParameter,
                choosedPlayerGirl.name
              );
              break;
            case "playerMan":
              const choosedPlayerMan = this.getRandomPlayerMan(choosedPlayers);

              choosedPlayers.push(choosedPlayerMan);

              questionText = questionText.replace(
                textParameter,
                choosedPlayerMan.name
              );
              break;
            case "number":
              if (textParameterT.length === 2) {
                textParameterT[1] = textParameterT[1].replace(/[)}]/g, "");

                const textParameterOptions = textParameterT[1].split(";");

                let minNumberStr = textParameterOptions.find((str) =>
                  str.includes("min:")
                );
                let maxNumberStr = textParameterOptions.find((str) =>
                  str.includes("max:")
                );

                if (minNumberStr !== undefined && maxNumberStr !== undefined) {
                  const minNumber = parseInt(minNumberStr.replace("min:", ""));
                  const maxNumber = parseInt(maxNumberStr.replace("max:", ""));

                  const generated = Helper.randomNumber(
                    minNumber,
                    maxNumber,
                    this.rng
                  );

                  questionText = questionText.replace(
                    textParameter,
                    generated.toString()
                  );
                }
              }

              break;

            default:
              console.error("parameter name not found", textParameterName);

              break;
          }
        }
      } catch (error) {
        console.error(error);
      }
    }

    return questionText;
  }

  public getNbDrinks() {
    return Helper.randomNumber(1, 6, this.rng);
  }

  public getRandomPlayer(
    alreadyChoosedPlayers: (User | DummyUser)[]
  ): User | DummyUser {
    const filteredUsers = this.allUsers.filter(
      (u) =>
        alreadyChoosedPlayers.find(
          (p) => p.id.toString() === u.id.toString()
        ) === undefined
    );

    return Helper.randomItem<User | DummyUser>(filteredUsers, this.rng);
  }

  public getRandomPlayerGirl(
    alreadyChoosedPlayers: (User | DummyUser)[]
  ): User | DummyUser {
    const filteredUsers = this.allUsers.filter(
      (u) =>
        alreadyChoosedPlayers.find(
          (p) => p.id.toString() === u.id.toString()
        ) === undefined && u.gender === 1
    );

    return Helper.randomItem<User | DummyUser>(filteredUsers, this.rng);
  }

  public getRandomPlayerMan(
    alreadyChoosedPlayers: (User | DummyUser)[]
  ): User | DummyUser {
    const filteredUsers = this.allUsers.filter(
      (u) =>
        alreadyChoosedPlayers.find(
          (p) => p.id.toString() === u.id.toString()
        ) === undefined && u.gender === 0
    );

    return Helper.randomItem<User | DummyUser>(filteredUsers, this.rng);
  }

  public static New(datas: Partial<Game>): Game {
    return Object.assign(new Game(), datas);
  }

  public static fromBack(datas: any) {
    let newObj = new Game();

    newObj.id = datas._id;
    newObj.displayId = datas._displayId;
    newObj.name = datas._name;
    for (const userModel of datas._users) {
      newObj.users.push(User.fromBack(userModel));
    }
    for (const dummyUserModel of datas._dummyUsers) {
      newObj.dummyUsers.push(DummyUser.fromBack(dummyUserModel));
    }

    newObj.nbTurns = datas._nbTurns;
    newObj.actualTurn = datas._actualTurn;
    for (const questionTypeModel of datas._questionTypes) {
      newObj.questionTypes.push(QuestionType.fromBack(questionTypeModel));
    }
    newObj.maxDifficulty = datas._maxDifficulty;
    newObj.maxHotLevel = datas._maxHotLevel;
    newObj.creator = User.fromBack(datas._creator);
    newObj.creationDate = new Date(datas._creationDate);
    newObj.type = GameType.fromBack(datas._type);
    newObj.status = datas._status;
    for (const scoreModel of datas._scores) {
      newObj.scores.push(Score.fromBack(scoreModel));
    }
    newObj.actualQuestion = Question.fromBack(datas._actualQuestion);

    return newObj;
  }

  public static CompareArrays(arr1: Game[], arr2: Game[]): boolean {
    return (
      arr1.length === arr2.length &&
      arr1.every((e, i) => Game.CompareObjects(e, arr2[i]))
    );
  }

  public static CompareObjects(obj1: Game, obj2: Game): boolean {
    return (
      obj1 !== undefined &&
      obj2 !== undefined &&
      obj1.id === obj2.id &&
      obj1.displayId === obj2.displayId &&
      obj1.name === obj2.name &&
      User.CompareArrays(obj1.users, obj2.users) &&
      obj1.nbTurns === obj2.nbTurns &&
      obj1.actualTurn === obj2.actualTurn &&
      QuestionType.CompareArrays(obj1.questionTypes, obj2.questionTypes) &&
      obj1.maxDifficulty === obj2.maxDifficulty &&
      obj1.maxHotLevel === obj2.maxHotLevel &&
      User.CompareObjects(obj1.creator, obj2.creator) &&
      obj1.creationDate === obj2.creationDate &&
      GameType.CompareObjects(obj1.type, obj2.type) &&
      obj1.status === obj2.status &&
      Score.CompareArrays(obj1.scores, obj2.scores) &&
      Question.CompareObjects(obj1.actualQuestion, obj2.actualQuestion)
    );
  }
}
