// store/session/actions.ts

import { ThunkAction, ThunkDispatch } from "redux-thunk";
import { AnyAction } from "redux";
import { GameActionTypes } from "./types";
import apiHandler from "../../api/apiHandler";
import { addError } from "../errors/actions";
import { Game } from "../../api/classes/game.class";

// Action Definition
export interface ActionStarted {
  type: GameActionTypes.ACTION_STARTED;
}
export interface ActionFailure {
  type: GameActionTypes.ACTION_FAILURE;
}
export interface StartLoading {
  type: GameActionTypes.START_LOADING;
}
export interface FinishLoading {
  type: GameActionTypes.FINISH_LOADING;
}
export interface Get {
  type: GameActionTypes.GET;
  game: Game;
}
export interface Update {
  type: GameActionTypes.UPDATE;
  game: Game;
}
export interface Remove {
  type: GameActionTypes.REMOVE;
}

// Union Action Types
export type Action =
  | ActionStarted
  | ActionFailure
  | StartLoading
  | FinishLoading
  | Get
  | Update
  | Remove;

export class GameActions {
  private static actionStartedCreator = (): ActionStarted => {
    return {
      type: GameActionTypes.ACTION_STARTED,
    };
  };

  private static actionFailureCreator = (): ActionFailure => {
    return {
      type: GameActionTypes.ACTION_FAILURE,
    };
  };

  public static startGameLoading = (): StartLoading => {
    return {
      type: GameActionTypes.START_LOADING,
    };
  };

  public static finishGameLoading = (): FinishLoading => {
    return {
      type: GameActionTypes.FINISH_LOADING,
    };
  };

  public static getGameById = (
    id: string
  ): ThunkAction<Promise<boolean>, {}, {}, AnyAction> => {
    return async (
      dispatch: ThunkDispatch<{}, {}, AnyAction>
    ): Promise<boolean> => {
      return new Promise<boolean>((resolve) => {
        dispatch(GameActions.actionStartedCreator());
        apiHandler.gameService.featherService
          .get(id)
          .then((game) => {
            dispatch({
              type: GameActionTypes.GET,
              game: game,
            });

            resolve(true);
          })
          .catch((error) => {
            dispatch(GameActions.actionFailureCreator());
            dispatch(addError(error));
            resolve(false);
          });
      });
    };
  };

  public static getGameByDisplayId = (
    displayId: string
  ): ThunkAction<Promise<boolean>, {}, {}, AnyAction> => {
    return async (
      dispatch: ThunkDispatch<{}, {}, AnyAction>
    ): Promise<boolean> => {
      return new Promise<boolean>((resolve) => {
        dispatch(GameActions.actionStartedCreator());
        apiHandler.gameService
          .findGameByDisplayId(displayId)
          .then((game) => {
            dispatch({
              type: GameActionTypes.GET,
              game: game,
            });

            resolve(true);
          })
          .catch((error) => {
            dispatch(GameActions.actionFailureCreator());
            dispatch(addError(error));
            resolve(false);
          });
      });
    };
  };
}

export const gameActionsInstance = new GameActions();
