FROM node:14.1-alpine AS app-builder

ARG REACT_APP_ENVIRONMENT
ENV REACT_APP_ENVIRONMENT $REACT_APP_ENVIRONMENT

RUN printenv

WORKDIR /usr/src/partydrink
COPY package.json package-lock.json ./
RUN npm install

COPY . ./
RUN npm run build

FROM node:14.1-alpine AS app

RUN printenv

WORKDIR /usr/src/partydrink-front

COPY --from=app-builder /usr/src/partydrink/build ./build

RUN npm install serve

CMD npx serve -s build -l 4269